<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'cmb_events_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_events_metaboxes( array $meta_boxes ) {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cmb_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $meta_boxes['test_metabox'] = array(
        'id'         => 'cmb_event_details',
        'title'      => __( 'Event Details', 'cmb' ),
        'pages'      => array( 'larkin_event', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        // 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
        'fields'     => array(
	    
            array(
                'name' => __( 'Event Date', 'cmb' ),
                'desc' => __( 'The date of the event', 'cmb' ),
                'id'   => $prefix . 'event_date',
                'type' => 'text_date_timestamp',
            ),
	    
            array(
                'name' => __( 'Event Start Time', 'cmb' ),
                'desc' => __( 'Time that the event starts', 'cmb' ),
                'id'   => $prefix . 'event_start_time',
                'type' => 'text_time',
		'time_format' => 'i'
            ),
	    
			array(
					'name' => __( 'Event End Time', 'cmb' ),
					'desc' => __( 'Time that the event ends', 'cmb' ),
					'id'   => $prefix . 'event_end_time',
					'type' => 'text_time',
			'time_format' => 'g A'
				),	             

			
			array(
                        'name' => 'Event Location',
                        'description' => 'Event Location - e.g. Hydraulic Hearth. Leave blank if Larkin Square',
                        'id'   => $prefix . 'event_location',
                        'type' => 'text',
                    ),
		)                    
			
    );

    /**
     * Metabox to be displayed on a single page ID
     */
    $meta_boxes['about_page_metabox'] = array(
        'id'         => 'about_page_metabox',
        'title'      => __( 'About Page Metabox', 'cmb' ),
        'pages'      => array( 'page', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
        'fields'     => array(
            array(
                'name' => __( 'Test Text', 'cmb' ),
                'desc' => __( 'field description (optional)', 'cmb' ),
                'id'   => $prefix . '_about_test_text',
                'type' => 'text',
            ),
        )
    );

    /**
     * Repeatable Field Groups
     */
    $meta_boxes['field_group'] = array(
        'id'         => 'field_group',
        'title'      => __( 'Repeating Field Group', 'cmb' ),
        'pages'      => array( 'page', ),
        'fields'     => array(
            array(
                'id'          => $prefix . 'repeat_group',
                'type'        => 'group',
                'description' => __( 'Generates reusable form entries', 'cmb' ),
                'options'     => array(
                    'group_title'   => __( 'Entry {#}', 'cmb' ), // {#} gets replaced by row number
                    'add_button'    => __( 'Add Another Entry', 'cmb' ),
                    'remove_button' => __( 'Remove Entry', 'cmb' ),
                    'sortable'      => true, // beta
                ),
                // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
                'fields'      => array(
                    array(
                        'name' => 'Entry Title',
                        'id'   => 'title',
                        'type' => 'text',
                        // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
                    ),
                    array(
                        'name' => 'Description',
                        'description' => 'Write a short description for this entry',
                        'id'   => 'description',
                        'type' => 'textarea_small',
                    ),
                    array(
                        'name' => 'Entry Image',
                        'id'   => 'image',
                        'type' => 'file',
                    ),
                    array(
                        'name' => 'Image Caption',
                        'id'   => 'image_caption',
                        'type' => 'text',
                    ),
                ),
            ),
        ),
    );

    /**
     * Metabox for the user profile screen
     */
    $meta_boxes['user_edit'] = array(
        'id'         => 'user_edit',
        'title'      => __( 'User Profile Metabox', 'cmb' ),
        'pages'      => array( 'user' ), // Tells CMB to use user_meta vs post_meta
        'show_names' => true,
        'cmb_styles' => false, // Show cmb bundled styles.. not needed on user profile page
        'fields'     => array(
            array(
                'name'     => __( 'Extra Info', 'cmb' ),
                'desc'     => __( 'field description (optional)', 'cmb' ),
                'id'       => $prefix . 'exta_info',
                'type'     => 'title',
                'on_front' => false,
            ),
            array(
                'name'    => __( 'Avatar', 'cmb' ),
                'desc'    => __( 'field description (optional)', 'cmb' ),
                'id'      => $prefix . 'avatar',
                'type'    => 'file',
                'save_id' => true,
            ),
            array(
                'name' => __( 'Facebook URL', 'cmb' ),
                'desc' => __( 'field description (optional)', 'cmb' ),
                'id'   => $prefix . 'facebookurl',
                'type' => 'text_url',
            ),
            array(
                'name' => __( 'Twitter URL', 'cmb' ),
                'desc' => __( 'field description (optional)', 'cmb' ),
                'id'   => $prefix . 'twitterurl',
                'type' => 'text_url',
            ),
            array(
                'name' => __( 'Google+ URL', 'cmb' ),
                'desc' => __( 'field description (optional)', 'cmb' ),
                'id'   => $prefix . 'googleplusurl',
                'type' => 'text_url',
            ),
            array(
                'name' => __( 'Linkedin URL', 'cmb' ),
                'desc' => __( 'field description (optional)', 'cmb' ),
                'id'   => $prefix . 'linkedinurl',
                'type' => 'text_url',
            ),
            array(
                'name' => __( 'User Field', 'cmb' ),
                'desc' => __( 'field description (optional)', 'cmb' ),
                'id'   => $prefix . 'user_text_field',
                'type' => 'text',
            ),
        )
    );

    /**
     * Metabox for an options page. Will not be added automatically, but needs to be called with
     * the `cmb_metabox_form` helper function. See wiki for more info.
     */
    $meta_boxes['options_page'] = array(
        'id'      => 'options_page',
        'title'   => __( 'Theme Options Metabox', 'cmb' ),
        'show_on' => array( 'key' => 'options-page', 'value' => array( $prefix . 'theme_options', ), ),
        'fields'  => array(
            array(
                'name'    => __( 'Site Background Color', 'cmb' ),
                'desc'    => __( 'field description (optional)', 'cmb' ),
                'id'      => $prefix . 'bg_color',
                'type'    => 'colorpicker',
                'default' => '#ffffff'
            ),
        )
    );

    // Add other metaboxes as needed

    return $meta_boxes;
}

add_action( 'init', 'cmb_init_events_metaboxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_init_events_metaboxes() {

    if ( ! class_exists( 'cmb_Meta_Box' ) )
        require_once 'init.php';

}
