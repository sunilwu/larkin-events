<?php
/*
Plugin Name: Larkin Events
Description: Events for Larkin Square website
Version: 0.4.0
Author: Sunil Williams
Author URI: http://sunil.co.nz
License: GPL
 */

if(!defined('ABSPATH')) {
    exit();
}

/**
/*set up constants
 */
define("LARKIN_EVENTS", "larkin-events")  ;

/**
/* this is the plug-in directory name
 */
if(!defined("LARKIN_EVENTS")) {
    define("LARKIN_EVENTS", trim(dirname(plugin_basename(__FILE__)), '/'));
}

/**
/* this is the path to the plug-in's directory
 */
if(!defined("LARKIN_EVENTS_DIR")) {
    define("LARKIN_EVENTS_DIR", WP_PLUGIN_DIR . '/' . LARKIN_EVENTS);
}

/**
 *  This is the url to the plug-in's directory
 */
if(!defined("LARKIN_EVENTS_URL")) {
    define("LARKIN_EVENTS_URL", WP_PLUGIN_URL . '/' . LARKIN_EVENTS);
}

/**
 * include our Custom Post Type
 */
include_once("cpt_larkin_events.php") ;


/**
 * include the Custom Meta Boxes And Fields library
 */

include_once("cmb/cmb-events-functions.php");


/**
 *   Call our styles and js supporting libs
 */
function larkin_events_scripts() {

    // jquery cycle2
    wp_enqueue_script( 'cycle2', LARKIN_EVENTS_URL . "/js/jquery.cycle2.min.js", 'jquery', true);

    //assets for cycle2
    $deps = array('jquery', 'cycle2') ;
    wp_enqueue_script( 'cycleswipe', LARKIN_EVENTS_URL . "/js/jquery.cycle2.swipe.min.js", $deps, true);
    wp_enqueue_script( 'cyclecar', LARKIN_EVENTS_URL . "/js/jquery.cycle2.carousel.min.js", $deps, true);

	// assets for slick
	wp_enqueue_script('jquery_migrate', 'http://code.jquery.com/jquery-migrate-1.2.1.min.js' ) ;	
	$slickdeps = array("jquery", "jquery_migrate") ;	
	wp_enqueue_script('slickjs', LARKIN_EVENTS_URL . "/js/slick.js", $slickdeps, true) ;
	wp_enqueue_style('slickcss',  LARKIN_EVENTS_URL . "/css/slick.css") ;	
	
    wp_enqueue_style('larkin_events_stylesheet',  LARKIN_EVENTS_URL . "/css/larkin-events.css") ;

	//finally, our own script
	$events_deps = array('jquery', 'slickjs' ) ;
	wp_enqueue_script('larkin-events-js', LARKIN_EVENTS_URL . "/js/larkin-events.js",  $events_deps,   true) ;
	
}
// add_action( 'wp_enqueue_scripts', 'larkin_events_scripts' );
add_action('wp_footer', 'larkin_events_scripts'); 

/**
/ *   Now that we've gotten things set up, we can output some actual php into wherever we want.
/ *   In our subdir loops we have some wordpress loops.
/ *   The Following functions will call these loops.
/ *   We can insert these functions into wherever  we want. The functions will spit out our loops
/ *
 */

/**
 *  This loop produces code for a slideshow on a smaller screen.
 * As long as the css is set up properly, it will be hidden on larger screens
 */
function larkinevents_small(){
    $out = include_once("loops/events_small.php") ;
    echo $out ;
}


/**
 *  This loop produces code for a slideshow on screens that are tablet sized and up
 * As long as the css is set up properly, it will be hidden on smaller screens
 */
function larkinevents_tplus(){
    $out = include_once("loops/events_tplus.php");
    echo $out ;
}

function larkinevents_list_all_events()
{
    $out = include_once("loops/events_list_all.php");
    echo $out ;
}

function larkinevents_list_all_past_events()
{
    $out = include_once("loops/events_list_all_past.php");
    echo $out ;
}

/*****
/*  output
/*
 */
function larkinevents_get_category_image() {

    $categories = get_the_category();

    if (!empty($categories)) {
        $cat_name =   $categories[0]->cat_name ;

        switch($cat_name) {

            case "Food Truck Tuesdays" :
                $image =  LARKIN_EVENTS_URL . "/img/category-images/food-truck-tuesdays.png";
                $image_class = "food-truck-tuesdays";
                break ;
            case "Author Series":
                $image =  LARKIN_EVENTS_URL . "/img/category-images/author-series.png";
                $image_class = "author-series";
                break ;
            case "Larkin Market":
                $image =  LARKIN_EVENTS_URL . "/img/category-images/larkin-market.png";
                $image_class = "larkin-market";
                break;
            case "Live At Larkin":
                $image =  LARKIN_EVENTS_URL . "/img/category-images/live-at-larkin.png";
                $image_class = "live-at-larkin" ;
                break;
            case "Lunchtime Live at Larkin":
                $image =  LARKIN_EVENTS_URL . "/img/category-images/lunchtime-live-at-larkin.png";
                $image_class = "lunchtime-live-at-larkin" ;
                break;
            case "Larkin Square Events":
                $image =  LARKIN_EVENTS_URL . "/img/category-images/larkin-square-events.png";
                $image_class = "larkin-square-events" ;
                break;
        }
        echo "<img src=\"{$image}\" class=\"{$image_class}\" />" ;
    }
    
}

/****
* For our slides, we want to link to their respective catagory pages
 */
function larkin_event_linkto_cat_page()
{

    
    // get the first category of the current post...
    $categories = get_the_category();
    $cat_name =   $categories[0]->cat_name ;

    // return a string, depending on what has been passed in
    switch($cat_name) {
	case "Live At Larkin" :
	    $cat_link = "live-at-larkin" ;
	    break;
	case "Lunchtime Live at Larkin" :
	    $cat_link = "live-at-larkin" ;
	    break;
	case "Food Truck Tuesdays" :
	    $cat_link = "things-to-do/food-truck-tuesdays" ;
	    break;
	case  "Author Series" :
	    $cat_link = "author-series" ;
	    break;
	case  "Larkin Market":
	    $cat_link = "things-to-do/larkin-marketplace" ;
	    break;
	case "Larkin Square Events":
	    $cat_link = "things-to-do" ;
	    break;
    }
    if ($cat_link != "") {
	    echo  get_site_url() . "/" .  $cat_link ;   
	}
}
