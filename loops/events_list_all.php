<?php
/****
/*  This file gives us a list of all events and all their details.
/*  It's purpose is to display the events and details on the 'events' page
 */

global $post ;
//date_default_timezone_set('America/New_York');
$timezone = date_default_timezone_get();
$today =  date('m/d/Y'); //time();
$today = strtotime($today);

$args = array('post_type' => 'larkin_event', 
			'meta_key' 	=> '_cmb_event_date',
			'meta_query'        => array (
				array (
				'key'       => '_cmb_event_date',
				'value'     => $today,
				'compare'   => '>=',
                'type' => 'NUMERIC'				
				)
			),
			'order'		=> 'ASC',
			'orderby'	=> 'meta_value',
			'posts_per_page' => -1

         ) ;
			
$my_query = new WP_Query($args) ;?>
<section id="events-list" class="group">
    <!-- calling the function 'events_list_all' -->
    <?php if ($my_query->have_posts()) :
     $count = 0 ;
    ?>
        <?php
        while ($my_query->have_posts()) : $my_query->the_post();
        $event_date = get_post_meta( $post->ID,  '_cmb_event_date' , true )  ;
        ?>
            <article class="event group<?php
                $count++ ;
                if ($count == 1) {
                    echo " first" ;
                } else {
                    $count = 0 ;
                }?>">
                <header class="group">
                    <div class="datetime group"  >
                        <span class="date"><?php
                                         if (!empty($event_date)) {
                                             echo  date( 'M j',  $event_date ) ;
                                         }?></span>
                        <span class="day"><?php
                                        if (!empty($event_date)) {
                                            echo date('l', $event_date) ;
                                        }?></span>
                    </div> <!-- ENDS .datetime -->
                    <div class="image-container">   <?php larkinevents_get_category_image() ?></div>
                </header>
                <div class="event-details">
                
<?php
/*date="<?php echo $event_date . '-' . gmdate("Y-m-d\TH:i:s\Z", $event_date);?>" today="<?php echo $today . '-' . gmdate("Y-m-d\TH:i:s\Z", $today);?>" */

        			$categories = get_the_category();

					if (!empty($categories)) {
						$cat_name =   $categories[0]->cat_name ;
						$start_time = get_post_meta( $post->ID,  '_cmb_event_start_time' , true)  ;
						$end_time = get_post_meta( $post->ID,  '_cmb_event_end_time' , true) ;
						$event_location = get_post_meta( $post->ID,  '_cmb_event_location' , true )  ;
					}

					switch($cat_name) {

						case "Food Truck Tuesdays" :
							$description =  "Food trucks from around Western New York park at Larkin Square every Tuesday from May through October. Come on down for your favorite food trucks, or try a new truck you haven’t visited yet. Enjoy live music, pickle ball, hula hoops and more.";
							$title = "Featured Live Act: ".get_the_title();
							break ;
						case "Author Series":
							$description =  "Featured Author: ".get_the_title();
							$title = "Author Talk - Q & A -  Signing - Beer, Wine, Light Fare for purchase"."<br/><br/>".get_the_content()."<br/><br/>Event Location: ".$event_location;
							break ;
						case "Larkin Market":
							$description =  "Come shop at our market featuring local and organic produce, artisan and craft vendors, as well as vintage goods every Thursday from July 10 through September 19.";
							$description .= '<br/><br/>For a list of the current week\'s vendors, <a href="/larkin-marketplace">click here</a>.';
							if (get_the_content() != '') {
								$title = "Featured: ".get_the_content();
							}
							else
							{
								$title = '';
							}
							break;
						case "Live At Larkin":
							$description =  "Enjoy live music every Wednesday evening from June 18 through September 17, featuring two bands playing live, local and original music.";
							$title = "Featured Live Acts: ".get_the_title();							
							break;
						case "Lunchtime Live at Larkin":
							$description =  "Lunchtime Live at Larkin presented by First Niagara and sponsored by Independent Health offers monthly live music on a Thursday from noon to 2:00 pm on the Boardwalk. Summertime food and beverages are available for purchase from the Larkin Grill.";
							if (get_the_content() != '') {
								$title = "Featured Live Acts: ".get_the_content();
							}
							else
							{
								$title = '';
							}
							break;
						case "Larkin Square Events":
							$description =  get_the_content();
							$title = "";
							$cat_name = get_the_title();
							break;


					}
					
?>                
                    <h3><?php echo $cat_name; ?></h3>
                    <p>
						<?php		
						  if (!empty($event_date)) {
						  echo date('l, F j', $event_date) ;
							  }   else{
							  echo "<p>no date set</p>" ;		  
							  }
						 ?></p>	      
							  <p><?php		  
							  if (!empty($start_time)) {
							  $str  = strtolower( $start_time );
							  $str = ltrim( $str, '0' ) ;
							  //$str = str_replace(array('am','pm'),array('a.m','p.m'),$str);
							  echo $str ;
							  }?>
						  <?php
						  if ( !empty($start_time) && !empty($end_time)  ) {
							  echo " - " ;
						  }
			  
						  if (!empty($end_time)) {
							  $str =  strtolower($end_time) ;
							  $str = ltrim( $str, '0' ) ;
							  //$str = str_replace(array('am','pm'),array('a.m','p.m'),$str);							  
							  echo $str ;		      
							  }
						 ?>
                    </p>


                <?php        
					echo '<div class="event-description">'.$description . '</div><div class="event-title">'.$title.'</div>';
				?>	
                </div>
            </article>
        <?php endwhile; ?>
    <?php endif; ?>
    <!-- loop has completed   -->
</section>
<?php return  ?>