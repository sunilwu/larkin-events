<?php
/**
   /*   This file gives us back the events slideshow for devices
   /*    'larger then mobile'.
   /*    For smaller devices, it gets hidden
   /*    Ideally we wouldn't deliver it all to smaller devices.
 */

global $post ;
//date_default_timezone_set('America/New_York');
$timezone = date_default_timezone_get();
$today =  date('m/d/Y'); //time();
$today = strtotime($today);

$args = array('post_type' => 'larkin_event',
              'meta_key'        => '_cmb_event_date',
              'meta_query'        => array (
    array (
	'key'       => '_cmb_event_date',
        'value'     => $today,
        'compare'   => '>=',
        'type' => 'NUMERIC'
    )
              ),
              'order'           => 'ASC',
              'orderby' => 'meta_value'

) ;
$my_query = new WP_Query($args) ;
if ($my_query->have_posts()) : ?>
    <div class="events-row tplus group">
        <div class="tplus-slick-display">
            <?php while ($my_query->have_posts()) : $my_query->the_post();
                  $event_date = get_post_meta( $post->ID,  '_cmb_event_date' , true )  ;
                  $start_time = get_post_meta( $post->ID,  '_cmb_event_start_time' , true)  ;
                  $end_time = get_post_meta( $post->ID,  '_cmb_event_end_time' , true) ;
            ?><div>
                <div class="image-container">
                    <a href="<?php larkin_event_linkto_cat_page();  ?>">
			<?php  larkinevents_get_category_image();   ?>
		    </a>
                </div>
                <div class="day">
                    <p><?php
                       if (!empty($event_date)) {
                           echo date('l, F j', $event_date) ;
                       }   else{
                           echo "<p>no date set</p>" ;
                       }
                       ?></p>
                    <p><?php
                       if (!empty($start_time)) {
                           $str  = strtolower( $start_time );
                           $str = ltrim( $str, '0' ) ;
                           //$str = str_replace(array('am','pm'),array('a.m','p.m'),$str);                        
                           echo $str ;
                       }?>
                        <?php
                        if ( !empty($start_time) && !empty($end_time)  ) {
                            echo " - " ;
                        }

                        if (!empty($end_time)) {
                            $str =  strtolower($end_time) ;
                            $str = ltrim( $str, '0' ) ;
                            //$str = str_replace(array('am','pm'),array('a.m','p.m'),$str);                   
                            echo $str ;
                        }
                        ?></p>
                </div>
            </div> <!-- ENDS slide -->
            <?php endwhile; ?>
        </div>  <!-- ENDS .tplus-slick-display -->
<?php endif; ?>

    </div>  <!-- ENDS .tplus -->  <?php return ;  ?>
