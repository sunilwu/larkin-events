<?php
/**
/*  This file gives us back the events slideshow
/*  for smaller devices
 */

global $post ;
//date_default_timezone_set('America/New_York');
$timezone = date_default_timezone_get();
$today =  date('m/d/Y'); //time();
$today = strtotime($today);

$args = array('post_type' => 'larkin_event', 
			'meta_key' 	=> '_cmb_event_date',
			'meta_query'        => array (
				array (
				'key'       => '_cmb_event_date',
				'value'     => $today,
				'compare'   => '>=',
                'type' => 'NUMERIC'				
				)
			),
			'order'		=> 'ASC',
			'orderby'	=> 'meta_value'

         ) ;
			
$my_query = new WP_Query($args);
if ($my_query->have_posts()) : ?>
    <div class="mobile-only">
        <div class="cycle-container">
            <?php // <div class="pointer pointer-prev"><span></span></div> ?>
            <ul class="cycle-slideshow  event-pane group"
                data-cycle-slides="li"
                data-cycle-prev=".pointer-prev"
                data-cycle-next=".pointer-next"
                data-cycle-pager=".events-pager"
                data-cycle-swipe=true
    			data-cycle-swipe-fx=scrollHorz>
                <?php while ($my_query->have_posts()) : $my_query->the_post();
                      $event_date = get_post_meta( $post->ID,  '_cmb_event_date' , true )  ;
                      $start_time = get_post_meta( $post->ID,  '_cmb_event_start_time' , true) ;
                      $end_time = get_post_meta( $post->ID,  '_cmb_event_end_time' , true) ;
                ?><li>
                    <div class="image-container">
                        <a href="<?php larkin_event_linkto_cat_page() ?>">
                            <?php  larkinevents_get_category_image() ;  ?></a>
                    </div>
                    <div class="day">
                        <p><?php
                           if (!empty($event_date)) {
                               echo  date( 'l, M j',  $event_date ) ;
                           } else {
                               echo "Date to be announced";
                           }
                           ?></p>
                        <p><?php
                           if (!empty($start_time)) {
                               $str =    strtolower($start_time) ;
			       $str = ltrim( $str, '0' ) ;
				   //$str = str_replace(array('am','pm'),array('a.m','p.m'),$str);			       
			       echo $str ;
                           } else {
                               echo "Start Time TBA";
                           }
			    // if both times are available, put in the wee dash
			    if (!empty($start_time  )  && !empty($end_time)) {
				echo " - " ;
			    }
			   
			    if (!empty($end_time)) {
					$str =   strtolower($end_time) ;
					$str = ltrim( $str, '0' ) ;
					//$str = str_replace(array('am','pm'),array('a.m','p.m'),$str);				
					echo $str ;
				} else {
					echo "Starting in the eventing </br>";
                            }
			    ?></p>
                    </div>
                </li>
                <?php endwhile; ?>
            </ul>
            <?php  // <div class="pointer pointer-next"><span></span></div>        ?>
            <div class="events-pager"></div>
        </div>  <!-- ENDS .cycle-container -->

    </div><?php endif;   return ;  ?>
