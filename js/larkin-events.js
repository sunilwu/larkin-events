jQuery(document).ready(function() {

    jQuery(".tplus-slick-display").slick(
        {
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay : true,
            arrows : true,
            dots : true,	    
	    draggable : true,
	    speed : 700,
	    swipe : true,
	    
        }) ;
});
