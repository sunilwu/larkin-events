<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays the front page
 *
 * @package larkin square
 */
get_header('frontpage'); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <article id="frontpage-events">
      <div class="inner-row title-container">
        <h2>Attend an Upcoming Event</h2>
      </div>
      <?php larkinevents_small() ; ?>
    </article>  <!-- ENDS #frontpage-events -->

    <article id="filling-station-intro" class="group">
      <header>
	<div class="delim"></div>             
      </header>
      
        <section id="filling-station-calls" class="group">
          <h2>Dine With Us</h2>
          <p class="group">
            <a href="" class="first" >Make a Reservation</a>
            <a href="" class="second">View Our Menu</a>
          </p>
        </section>

    </article>  <!-- ENDS #filling-station-intro -->

    <article id="sponsers">
      <div class="inner-row title-container"><h2>Our Sponsors</h2></div>
      <div class="inner-row">
        <div class="sponser-container">
        <section>
	  <span class="first">
	    <a href="#" alt="First Niagara">
	    <img src="<?php echo get_template_directory_uri()  ?>/img/sponsors/first-niagara.jpg"
		 alt="First Niagara"/>
	    </a>
	  </span>
        </section>
        <section>
	  <span class="second">
            <a href="#" alt="Independant Health">
	    <img src="<?php echo get_template_directory_uri()  ?>/img/sponsors/independent-health.jpg"
		 alt="First Niagara"/>
	    </a>
	  </span>
        </section>
	</div>
      </div>
    </article>

  </main><!-- ENDS #main -->
</div><!-- ENDS #primary -->

<?php get_footer(); ?>
